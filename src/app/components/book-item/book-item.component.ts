import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { Book } from '../../shared/book';


@Component({
  selector: 'bm-book-item',
  templateUrl: './book-item.component.html',
  styles: ['.ui.card.book {margin-bottom: 8px;}'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookItemComponent implements OnInit {

  @Input() book: Book;
  @Output() added = new EventEmitter<Book>();
  constructor() { }

  ngOnInit() {
  }

}
