import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { Book } from '../../shared/book';

@Component({
  selector: 'bm-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookFormComponent implements OnInit {

  @Output() created = new EventEmitter<Book>();
  constructor() { }

  submitted(form){
    this.created.emit(
      new Book(
        form.controls['isbn'].value,
        form.controls['title'].value,
        form.controls['price'].value
      )
    )
  }

  ngOnInit() {
  }

}
