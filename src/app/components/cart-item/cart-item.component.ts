import { Component, OnInit, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';
import { Book } from '../../shared/book';

@Component({
  selector: 'bm-cart-item',
  templateUrl: './cart-item.component.html',
  styles: ['.ui.red.cart.item {margin-botttom: 8px;}'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CartItemComponent implements OnInit {

  @Input() item: Book;
  @Input() index: number;
  @Output() removed = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

}
