import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import { select } from "@angular-redux/store";
import { Observable } from "rxjs/Observable";

import { IAppState } from "./../../app.state";
import { BookActions  } from "./../../actions/book";
import { Book } from '../../shared/book';
import { CartActions } from '../../actions/cart';
import { UUID } from 'angular2-uuid';

@Component({
  selector: 'bm-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit {

  // Wenn Änderungen registriert werden...
  @select((state: IAppState) => state.books.all) books$:Observable<Book[]>;

  constructor(
    private books: BookActions,
    private cart: CartActions
  ) { }

  ngOnInit() {
  }

  addBook(book:Book){
    this.books.addSingle(book);
  }

  addBookToCart(book: Book){
    this.cart.createSingle(book);
  }
}
