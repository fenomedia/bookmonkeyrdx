import { Component, ChangeDetectionStrategy } from '@angular/core';
import { select } from '@angular-redux/store';
import { IAppState } from '../../app.state';
import { Observable } from "rxjs/observable";
import { Book } from '../../shared/book';
import { CartActions } from '../../actions/cart';

@Component({
  selector: 'bm-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CartComponent {

  @select((state:IAppState) => state.cart.total) total$: Observable<number>;
  @select((state:IAppState) => state.cart.items) items$: Observable<Book[]>;

  constructor(private cart: CartActions) { }

  removeItemFromCart(index: number){
    this.cart.removeSingle(index);
  }

  addItemToCart(book: Book){
    this.cart.createSingle(book);
  }

}
