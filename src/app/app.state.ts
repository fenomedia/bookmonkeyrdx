import { Book } from './shared/book';

export interface IBookState{
    all: Book[];
}

export interface ICartState{
    items: Book[];
    total: number;
}

export interface IAppState{
    books: IBookState;
    cart: ICartState;
}