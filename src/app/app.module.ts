import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgRedux, NgReduxModule, DevToolsExtension  } from '@angular-redux/store';
import { FormsModule } from "@angular/forms";


import { AppComponent } from './app.component';
import { DashboardComponent } from './containers/dashboard/dashboard.component';
import { CartComponent } from './containers/cart/cart.component';
import { BookActions } from './actions/book';
import { CartActions } from './actions/cart';

import { reducer } from './reducers/index';
import { IAppState } from './app.state';
import { BookFormComponent } from './components/book-form/book-form.component';
import { BookItemComponent } from './components/book-item/book-item.component';
import { CartItemComponent } from './components/cart-item/cart-item.component';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    CartComponent,
    BookFormComponent,
    BookItemComponent,
    CartItemComponent
  ],
  imports: [
    BrowserModule,
    NgReduxModule,
    FormsModule
  ],
  providers: [
    BookActions,
    CartActions,
    DevToolsExtension
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    private ngRedux: NgRedux<IAppState>,
    private devTools: DevToolsExtension
  ){
    let enhancers = [];
    if(environment.production == false && devTools.isEnabled()){
      enhancers = [...enhancers, devTools.enhancer()];
    }
    this.ngRedux.configureStore(reducer, {} as IAppState, [], enhancers);
  }
 }
